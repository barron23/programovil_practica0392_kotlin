package com.example.practica0392_kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

private lateinit var btnSumar : Button
private lateinit var btnRestar : Button
private lateinit var btnMulti : Button
private lateinit var btnDiv : Button
private lateinit var btnLimpiar : Button
private lateinit var btnRegresar : Button
private lateinit var lblUsuario : TextView
private lateinit var lblResultado : TextView
private lateinit var txtUno : EditText
private lateinit var txtDos : EditText

//Declarar el objeto Calculadora
private var calculadora = Calculadora(0,0)

class CalculadoraActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calculadora)
        iniciarComponentes()

        //Obtener los datos del mainActivity
        //Enviar datos del main al calculadoraActivity, usuario y contraseña
        var datos = intent.extras
        var usuario = datos!!.getString("usuario")
        lblUsuario.text = usuario.toString()

        btnSumar.setOnClickListener { btnSumar() }
        btnRestar.setOnClickListener { btnRestar() }
        btnMulti.setOnClickListener { btnMultiplicacion() }
        btnDiv.setOnClickListener { btnDividir() }

        btnLimpiar.setOnClickListener { btnLimpiar() }
        btnRegresar.setOnClickListener { btnRegresar() }
    }

    private fun iniciarComponentes() {
        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMulti = findViewById(R.id.btnMultiplicar)
        btnDiv = findViewById(R.id.btnDividir)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnRegresar = findViewById(R.id.btnRegresar)

        lblUsuario = findViewById(R.id.lblUsuario)
        lblResultado = findViewById(R.id.lblResultado)

        txtUno = findViewById(R.id.txtNum1)
        txtDos = findViewById(R.id.txtNum2)

    }

    //Funcion boton sumar
    fun btnSumar() {
        if (txtUno.text.toString().equals("") || txtDos.text.toString().equals("")) {
            Toast.makeText(this.applicationContext, "Complete los campos", Toast.LENGTH_SHORT).show()
        } else {
            //Toma el texto que hay en los edittext, los convierte a string y despues a entero, despues manda a
            //llamar el metodo suma que esta en calculadora.kt y por ultimo muestra en la etiqueta resultado el total
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.suma()
            lblResultado.text = total.toString()
        }
    }

    //Funcion boton restar
    fun btnRestar() {
        if (txtUno.text.toString().equals("") || txtDos.text.toString().equals("")) {
            Toast.makeText(this.applicationContext, "Complete los campos", Toast.LENGTH_SHORT).show()
        } else {
            //Toma el texto que hay en los edittext, los convierte a string y despues a entero, despues manda a
            //llamar el metodo suma que esta en calculadora.kt y por ultimo muestra en la etiqueta resultado el total
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.resta()
            lblResultado.text = total.toString()
        }
    }

    //Funcion boton multiplicar
    fun btnMultiplicacion() {
        if (txtUno.text.toString().equals("") || txtDos.text.toString().equals("")) {
            Toast.makeText(this.applicationContext, "Complete los campos", Toast.LENGTH_SHORT).show()
        } else {
            //Toma el texto que hay en los edittext, los convierte a string y despues a entero, despues manda a
            //llamar el metodo suma que esta en calculadora.kt y por ultimo muestra en la etiqueta resultado el total
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.multiplicacion()
            lblResultado.text = total.toString()
        }
    }

    //Funcion boton dividir
    fun btnDividir() {
        if (txtUno.text.toString().equals("") || txtDos.text.toString().equals("")) {
            Toast.makeText(this.applicationContext, "Complete los campos", Toast.LENGTH_SHORT).show()
        } else {
            //Toma el texto que hay en los edittext, los convierte a string y despues a entero, despues manda a
            //llamar el metodo suma que esta en calculadora.kt y por ultimo muestra en la etiqueta resultado el total
            calculadora.num1 = txtUno.text.toString().toInt()
            calculadora.num2 = txtDos.text.toString().toInt()
            var total = calculadora.division()
            lblResultado.text = total.toString()
        }
    }

    fun btnLimpiar() {
        lblResultado.setText("")
        txtUno.setText("")
        txtDos.setText("")
    }

    fun btnRegresar() {
        //Muestra alerta para cerrar la ventana
        var confirmar = AlertDialog.Builder(this)
        //Titulo de la ventana
        confirmar.setTitle("Calculadora")
        //Muestra una "pregunta"
        confirmar.setMessage("¿Desea regresar al MainActivity?")
        //Codificacion de los botones,      which nos envia hacia un metodo, en este caso regresa al main
        confirmar.setPositiveButton("Confirmar") { dialogInterface, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialogInterface, which -> }
        confirmar.show()
    }

}