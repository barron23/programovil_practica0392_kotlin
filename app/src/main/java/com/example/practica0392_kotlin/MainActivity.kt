package com.example.practica0392_kotlin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class MainActivity : AppCompatActivity() {
    private lateinit var btnIngresar : Button
    private lateinit var btnSalir : Button
    private lateinit var txtUsuario : EditText
    private lateinit var txtContraseña : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        btnIngresar.setOnClickListener { ingresar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes(){
        btnIngresar = findViewById(R.id.btnIngresar)
        btnSalir = findViewById(R.id.btnSalir)
        txtUsuario = findViewById(R.id.txtUsuario)
        txtContraseña = findViewById(R.id.txtContraseña)
    }

    private fun ingresar(){
        val strUsuario : String
        val strContraseña : String

        //Relacionar lo que introduce el usuario desde login con los valores del archivo values > string.xml
        strUsuario = applicationContext.resources.getString(R.string.usuario)
        strContraseña = applicationContext.resources.getString(R.string.contraseña)

        //VALIDAR LOGIN
        if(strUsuario.equals(txtUsuario.text.toString()) && strContraseña.equals(txtContraseña.text.toString())){

            //Hacer el paquete para enviar la informacion
            val bundle = Bundle();
            bundle.putString("usuario",txtUsuario.text.toString())

            //Hacer intent para llamar otra actividad
            //                              origen              destino         de la peticion
            val intent = Intent(this@MainActivity,CalculadoraActivity::class.java)
            //enviando el usuario
            intent.putExtras(bundle)

            //Iniciar la actividad - esperando o no respuesta
            startActivity(intent)


        }else {
            Toast.makeText(this.applicationContext,"El usuario o contraseña no son válidos", Toast.LENGTH_SHORT).show()
        }

    }

    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("CALCULADORA")
        confirmar.setMessage("¿Desea cerrar la app?")
        confirmar.setPositiveButton("Confirmar"){dialogInterface,which->finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface,which->}
        confirmar.show()
    }




}



